FROM node:14

WORKDIR /app/

COPY package*.json /app/
COPY tsconfig*.json /app/
RUN npm install

COPY ./src /app/

RUN npm run build 
CMD [ "node","dist/main" ]